#ifndef FAKEWINDSPEEDSENSOR_H
#define FAKEWINDSPEEDSENSOR_H

#include <QObject>

#include "iwindspeedsensor.h"
#include "fakewindspeedsensorihm.h"

class FakeWindSpeedSensor : public IWindSpeedSensor
{
public:
    FakeWindSpeedSensor();
    ~FakeWindSpeedSensor();

    double getWindSpeed() override;

private:
    FakeWindSpeedSensorIHM * m_ihm;
    double m_windSpeed;

private slots :
    void onWindSpeedChanged(double windSpeed);
};

#endif // FAKEWINDSPEEDSENSOR_H
