#ifndef IWINDSPEEDSENSOR_H
#define IWINDSPEEDSENSOR_H

#include <QObject>

class IWindSpeedSensor : public QObject
{
    Q_OBJECT
public:
    explicit IWindSpeedSensor(QObject *parent = nullptr);

    virtual double getWindSpeed() = 0;

    virtual ~IWindSpeedSensor();
};

#endif // IWINDSPEEDSENSOR_H
