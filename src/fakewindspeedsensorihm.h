#ifndef FAKEWINDSPEEDSENSORIHM_H
#define FAKEWINDSPEEDSENSORIHM_H

#include <QDialog>

namespace Ui {
class FakeWindSpeedSensorIHM;
}

class FakeWindSpeedSensorIHM : public QDialog
{
    Q_OBJECT

public:
    explicit FakeWindSpeedSensorIHM(QWidget *parent = nullptr);
    ~FakeWindSpeedSensorIHM();

private:
    Ui::FakeWindSpeedSensorIHM *ui;

signals:
    void windSpeedChanged(double speed);

private slots:
    void onSliderWindSpeedValueChanged(int value);
};

#endif // FAKEWINDSPEEDSENSORIHM_H
