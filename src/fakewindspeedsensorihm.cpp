#include "fakewindspeedsensorihm.h"
#include "ui_fakewindspeedsensorihm.h"

FakeWindSpeedSensorIHM::FakeWindSpeedSensorIHM(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FakeWindSpeedSensorIHM)
{
    ui->setupUi(this);

    connect(ui->sliderWindSpeed, &QSlider::valueChanged
            , this, &FakeWindSpeedSensorIHM::onSliderWindSpeedValueChanged
            );
}

FakeWindSpeedSensorIHM::~FakeWindSpeedSensorIHM()
{
    delete ui;
}

void FakeWindSpeedSensorIHM::onSliderWindSpeedValueChanged(int value)
{
    double windSpeed = value / 10.0;

    ui->lcdWindSpeed->display(windSpeed);

    emit windSpeedChanged(windSpeed);
}
