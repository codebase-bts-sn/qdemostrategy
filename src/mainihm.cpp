#include "mainihm.h"
#include "ui_mainihm.h"


MainIHM::MainIHM(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainIHM)
{
    ui->setupUi(this);

    connect(ui->btnDemarrer, &QPushButton::clicked,
            this, &MainIHM::onBtnDemarrerClicked
            );

    m_timer = new QTimer();

    connect(m_timer, &QTimer::timeout, this, &MainIHM::onTimerExpired);

    m_timer->setInterval(2000); // intervalle 2s
}

MainIHM::~MainIHM()
{
    delete ui;
}

void MainIHM::onBtnDemarrerClicked(bool checked)
{
    if(checked) {
        // Portion de code qui sélectionne la "strategy" selon IHM
        if(ui->rbtnSimule->isChecked()) {
            m_sensor = new FakeWindSpeedSensor();
        } else {
            m_sensor = new RealWindSpeedSensor();
        }

        m_timer->start();

        ui->btnDemarrer->setText("Stopper surveillance");
        ui->gpbxChoixCapteur->setEnabled(false);
    } else {
        m_timer->stop();

        delete m_sensor;

        ui->btnDemarrer->setText("Démarrer surveillance");
        ui->gpbxChoixCapteur->setEnabled(true);
        ui->lcdVitesseVent->display(0.0);
        ui->lblBeaufort->setText("<correspondance échelle de Beaufort>");
    }

}

void MainIHM::onTimerExpired()
{
    double windSpeed = m_sensor->getWindSpeed();


    windSpeed *= 3.6; // vitesse en km/h

    ui->lcdVitesseVent->display(windSpeed);

    if((windSpeed >= 0) && (windSpeed < 1)) {
        ui->lblBeaufort->setText("Calme");
    } else if((windSpeed >= 1) && (windSpeed < 5)) {
        ui->lblBeaufort->setText("Brise très légère");
    } else if((windSpeed >= 5) && (windSpeed < 11)) {
        ui->lblBeaufort->setText("Brise légère");
    } else if((windSpeed >= 11) && (windSpeed < 19)) {
        ui->lblBeaufort->setText("Petite brise");
    } else if((windSpeed >= 19) && (windSpeed < 28)) {
        ui->lblBeaufort->setText("Jolie brise");
    } else if((windSpeed >= 28) && (windSpeed < 38)) {
        ui->lblBeaufort->setText("Bonne brise");
    } else if((windSpeed >= 38) && (windSpeed < 50)) {
        ui->lblBeaufort->setText("Vent frais");
    } else if((windSpeed >= 50) && (windSpeed < 61)) {
        ui->lblBeaufort->setText("Grand vent");
    } else if((windSpeed >= 61) && (windSpeed < 74)) {
        ui->lblBeaufort->setText("Coup de vent");
    } else if((windSpeed >= 74) && (windSpeed < 88)) {
        ui->lblBeaufort->setText("Fort coup de vent");
    } else if((windSpeed >= 88) && (windSpeed < 102)) {
        ui->lblBeaufort->setText("Tempête");
    } else if((windSpeed >= 102) && (windSpeed < 117)) {
        ui->lblBeaufort->setText("Violente tempête");
    } else if(windSpeed >= 117) {
        ui->lblBeaufort->setText("Ouragan");
    } else {
        ui->lblBeaufort->setText("vitesse invalide");
    }

}

