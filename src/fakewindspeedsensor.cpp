#include "fakewindspeedsensor.h"

FakeWindSpeedSensor::FakeWindSpeedSensor() : IWindSpeedSensor()
{
    m_windSpeed = 0;

    m_ihm = new FakeWindSpeedSensorIHM();
    m_ihm->show();
    m_ihm->move(0,0);

    connect(m_ihm, &FakeWindSpeedSensorIHM::windSpeedChanged
            , this, &FakeWindSpeedSensor::onWindSpeedChanged
            );

}

FakeWindSpeedSensor::~FakeWindSpeedSensor()
{
    delete m_ihm;
}

double FakeWindSpeedSensor::getWindSpeed() {
    return m_windSpeed;
}

void FakeWindSpeedSensor::onWindSpeedChanged(double windSpeed)
{
    m_windSpeed = windSpeed;
}
