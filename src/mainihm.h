#ifndef MAINIHM_H
#define MAINIHM_H

#include <QDialog>
#include <QTimer>

#include "fakewindspeedsensor.h"
#include "realwindspeedsensor.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainIHM; }
QT_END_NAMESPACE

class MainIHM : public QDialog
{
    Q_OBJECT

public:
    MainIHM(QWidget *parent = nullptr);
    ~MainIHM();

private:
    Ui::MainIHM *ui;

    IWindSpeedSensor * m_sensor;

    QTimer * m_timer;

private slots :
    void onBtnDemarrerClicked(bool checked);
    void onTimerExpired();
};
#endif // MAINIHM_H
