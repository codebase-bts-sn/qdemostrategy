#include <QDebug>
#include <QtNetwork/QNetworkRequest>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "realwindspeedsensor.h"

RealWindSpeedSensor::RealWindSpeedSensor()
{
    connect(&m_timer, &QTimer::timeout, this, &RealWindSpeedSensor::onTimerTimeout);

    connect(&m_nam, &QNetworkAccessManager::finished
            , this, &RealWindSpeedSensor::onReplyFinished
            );

    m_nam.connectToHost("http://api.openweathermap.org");

    m_nr.setUrl(QUrl("http://api.openweathermap.org/data/2.5/weather?q=L'Isle-sur-la-sorgue,fr&appid=<VOTRE-CLE-D-API>"));
    m_nr.setHeader(QNetworkRequest::ContentTypeHeader, QString("application/json"));

    m_timer.setInterval(1000 * 3600); // timeout toutes les heures
    m_timer.start();

    // Force l'acquisition maintenant
    onTimerTimeout();
}

double RealWindSpeedSensor::getWindSpeed()
{
    return m_windSpeed;
}

void RealWindSpeedSensor::onReplyFinished(QNetworkReply *reply)
{
    QByteArray response = reply->readAll();

    QJsonDocument jdoc = QJsonDocument::fromJson(response);
    QJsonObject jobj = jdoc.object();

    //QStringList keys = jobj.keys();
    //qDebug() << "keys : " << keys;
    QJsonObject jwind = jobj.value("wind").toObject();
    QJsonValue jval = jwind.value("speed");
    //qDebug() << "jval :" << jval;

    m_windSpeed = jval.toDouble();
    //qDebug() << "windSpeed :" << windSpeed;

}

void RealWindSpeedSensor::onTimerTimeout()
{
    m_nrep = m_nam.get(m_nr);
}
