#ifndef REALWINDSPEEDSENSOR_H
#define REALWINDSPEEDSENSOR_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QTimer>

#include "iwindspeedsensor.h"

class RealWindSpeedSensor : public IWindSpeedSensor
{
public:
    RealWindSpeedSensor();
    double getWindSpeed() override;

private:
    QNetworkAccessManager m_nam;
    QNetworkRequest m_nr;
    QNetworkReply * m_nrep;
    double m_windSpeed;
    QTimer m_timer;

private slots:
    void onReplyFinished(QNetworkReply *reply);
    void onTimerTimeout();
};

#endif // REALWINDSPEEDSENSOR_H
