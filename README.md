
# QDemoStrategy

Exemple d'application *Qt* illustrant le *design pattern*  ***Strategy***.

L'application donne la correspondance en km/s et sur l'échelle de Beaufort d'une vitesse de vent exprimée en m/s.

La vitesse de vent utilisée pour la conversion provient, selon le choix fait dans l'IHM principale, soit d'un capteur simulé par un slider *Qt* soit d'une mesure réelle relevée par le site *[OpenWeather](https://openweathermap.org/city/2970554)*.

Le *design pattern* ***Strategy*** est alors appliqué pour accéder, de manière identique, à la vitesse du vent en m/s quel que soit le choix de l'utilisateur (vent simulé ou vent réellement relevé).

![alt text](img/strategy-solution.svg  "Design pattern Strategy")

Ce *design pattern* a une réelle utilité en phase de projet lorsqu'on développe le code d'une application et que le choix du capteur définitif n'est pas encore arrété, qu'il n'est pas diponible ou qu'il est difficile de lui faire délivrer des valeurs sur toute sont étendue de mesure. On peut alors le simuler à travers une IHM dédiée pour tester le comportement de l'application lorsqu'il délivre, par exemple, des valeurs extrèmes (-> dépassements de seuils min. et/ou max.).

NOTE : Pour faire fonctionner le programme :
1. créer un compte sur *[OpenWeather](https://openweathermap.org/city/2970554)*
2. créer une clé d'API
3. renseigner sa valeur à la place du texte générique `<VOTRE-CLE-D-API>` présent dans le fichier `realwindspeedsensor.cpp`

![alt text](img/qdemostrategy.png  "Qt App screenshot")